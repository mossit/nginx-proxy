FROM jwilder/nginx-proxy
MAINTAINER Thiago Almeida thiagoalmeidasa@gmail.com
RUN { \
      echo 'server_tokens off;'; \
      echo 'client_max_body_size 64m;'; \
    } > /etc/nginx/conf.d/my_proxy.conf
